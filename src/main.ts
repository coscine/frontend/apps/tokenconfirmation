import jQuery from 'jquery';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@itcenter-layout/bootstrap/dist/css/material-icons.css';
import '@itcenter-layout/masterpage/dist/css/itcenter-masterdesign-masterpage.css';
import '@itcenter-layout/bootstrap/dist/css/rwth-theme.css';
import Vue from 'vue';
import TokenConfirmationApp from './TokenConfirmationApp.vue';
import VueI18n from 'vue-i18n';
import locales from './locale/locales';

Vue.config.productionTip = false;
Vue.use(VueI18n);

let localeValue = ((navigator as any).language || (navigator as any).userLanguage) as string;
if (localeValue.indexOf('de') !== -1) {
  localeValue = 'de';
} else {
  localeValue = 'en';
}

jQuery(() => {
    const i18n = new VueI18n({
      locale: localeValue,
      messages: locales,
      silentFallbackWarn: true,
    });

    new Vue({
      render: (h) => h(TokenConfirmationApp),
      i18n,
    }).$mount('tokenconfirmation');
});
