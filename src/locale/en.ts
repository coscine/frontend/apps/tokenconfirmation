export default {
    headline: 'Change contact information:',
    loadingSpinnerTokenConfirmation: 'Validating ...',
    invalidToken: 'The supplied token is invalid.', 
    validatedTokenSuccess: 'Validation successful.',
    validatedTokenFailure: 'The supplied token is invalid or was already used.',
    validating: 'Validating ...',

    localeValue: 'En',
    help: 'Help',
    disclaimer: 'Disclaimer',
    imprint: 'Imprint',
    contact: 'Contact',
    otherLocale: 'De',
};
