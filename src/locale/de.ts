export default {
    headline: 'Kontaktinformationen ändern:',
    loadingSpinnerTokenConfirmation: 'Wird geprüft ...',
    invalidToken: 'Das Token ist ungültig.', 
    validatedTokenSuccess: 'Validierung erfolgreich.',
    validatedTokenFailure: 'Das verwendete Token ist ungültig oder wurde bereits genutzt.',
    validating: 'Wird geprüft ...',

    localeValue: 'De',
    help: 'Hilfe',
    disclaimer: 'Datenschutz',
    imprint: 'Impressum',
    contact: 'Kontakt',
    otherLocale: 'En',
};
